/*insertDomElement: creates and inserts an element into the document
  parameters:
  elementType: a string representing the name of the element type you wish to create. if this is '_text_', a text node will be created instead of an element.
  content: a string representing the inner HTML of the element
  parent: the Node you would like to insert the element into
*/
function insertDOMElement(elementType, content, parent = document.body) {
	let element;
	if (elementType == "_text_") {
		element = document.createTextNode(content);
	}
	else {
		element = document.createElement(elementType);
		element.innerHTML = content;
	}
	parent.appendChild(element);
	return element;
}

/*getParams: returns an associative array of the page's GET values*/
function getParams()
{
    let result = {},
		tmp = [];

    location.search
        .substr(1)
        .split("&")
        .forEach(function(item)
        {
            tmp = item.split("=");
            result[tmp[0]] = decodeURIComponent(tmp[1]);
        });

    return result;
}
location.getParams = getParams;

/*getParamsText: returns the text of the page's GET values*/
function getParamsText() {
	return location.search.substr(1);
}
location.getParamsText = getParamsText;

/*alterSearchValue: changes the current window's location to include the specified search value, without reloading the page.
  parameters:
  value: the value to set the search location to; defaults to an empty string.
*/
function alterSearchValue(value = "") {
	let newurl = window.location.protocol + "//" +
		window.location.host +
		window.location.pathname +
		value;
	window.history.pushState({path:newurl},'',newurl);
}
location.alterSearchValue = alterSearchValue;

/*isUndefined: checks if the given variable is undefined
  variable: the variable to be checked
*/
function isUndefined(variable) {
	return (typeof variable == 'undefined');
}