---
Title: 'Draconic Infinity: Chapter 2'
Author: 'NafriUkgnet'
Description: 'The successor of Draconic Infinity Modpack'
Image: 'draconic-infinity-chapter-2.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/draconic-infinity-chapter-2'
Categories: ['exploration','hardcore','magic','tech','pvp']
---
Welcome to Draconic Infinity Chapter 2, the successor of Draconic Infinity Modpack but in the way that you've never seen before

Key Features :

*   There are more than 20 new dimensions to explore and conquer, thanks to [Advent of Ascension](https://www.curseforge.com/minecraft/mc-mods/advent-of-ascension-nevermine) mod
*   Play with the new 1.16 mods along with the old 1.12 mods that has been ported
*   This modpack has no main goal, because I want it to be like the old school kitchensink modpack
*   Even more beautiful world generation with Biomes You Go

To play this pack, you have to allocate at least 5GB RAM. And I don't recommend to use shaders since the mods can break it

Join Our Discord Server : [https://discord.gg/m9NTMV2NHq](https://discord.gg/m9NTMV2NHq)

Are you tired of being lonely and want to play with your friends

[![](https://www.bisecthosting.com/partners/custom-banners/f4c293a2-9b0d-4670-a27e-e719dc2a6625.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fnaffynafnaf)  Click on the picture above, select plan (at least 5GB), use my code **naffynafnaf** to get **25%** off your first month and enjoy playing with your friends!

[![](https://media.discordapp.net/attachments/862012820325269524/862047906310914078/Ring-Packs-Affiliated-Modpacks-Banner_Zeichenflache_1.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)
