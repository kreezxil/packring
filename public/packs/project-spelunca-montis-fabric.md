---
Title: 'Project Spelunca Montis Fabric'
Author: 'thenimbleninja'
Description: 'Tons and tons of things!'
Image: 'project-spelunca-montis-fabric.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/project-spelunca-montis-fabric'
Categories: ['adventure','exploration','light']
---
![](https://www.bisecthosting.com/images/CF/Spelunca_Montis/BH_SM_Header.png)

** ![](https://www.bisecthosting.com/images/CF/Spelunca_Montis/BH_SM_Overview.png)**

**Requires Java 16 or above to play this pack!**

Project Spelunca Montis Fabric is a the latest and greatest minecraft modpack! it uses all the new and greatest fabric mods on 1.17! i will continue to add mods till i cant add them no more!

there are tons of building blocks to choose from, tons of options for food, tons of armor and weapons to choose, and a few options for tech.

currently has few recipes changes and new stack size of 2048

![](https://www.bisecthosting.com/images/CF/Spelunca_Montis/BH_SM_Socials.png)

[![](https://i.imgur.com/4k8lKdV.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.guilded.gg%252fi%252fRkrdW7lE)

[![](https://www.bisecthosting.com/images/CF/social_assets/BH_Patreon.png)](https://www.patreon.com/NinjaTheNimble)

[![](https://cdn.discordapp.com/attachments/862061100933972038/865086703261974598/packring-logo.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)

                                      **Project Spelunca Monits Fabric has partnered with BisectHosting! **

[![](https://www.bisecthosting.com/images/CF/Spelunca_Montis/BH_SM_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fTheNimbleNinja)

**BisectHosting** offers quick, simple, and high-quality server hosting with over a dozen locations available across the world, ensuring low                                     latency. After your purchase, the server is already configured and ready to use.

           Use the code **"TheNimbleNinja"** to get **25% OFF** your first month as a new client for any of their gaming servers.
