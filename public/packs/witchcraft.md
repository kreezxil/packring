---
Title: 'WitchCraft'
Author: 'Hate9'
Description: 'A freeform magic-focused pack with a variety of custom content'
Image: 'witchcraft.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/witchcraft-dimensions'
Categories: ['magic', 'exploration', 'multiplayer']
Color: 190
---
**Travel the multiverse in this magic-focused modpack!**

 

![WitchCraft](https://media.forgecdn.net/attachments/313/616/title.png)

 

While others try to master the universe  through the use of technology, you have chosen to paint your name across the multiverse with MAGIC!

Make use of a wide variety of magic  systems and tools to travel and master a massive array of dimensions.  Fly above the clouds! Create your own pocket dimension! Enchant your  friends!

 

Proudly sponsored by BisectHosting. Use code "hate" for 25% off your first month!

They also have WitchCraft as a preinstall option, so you can get a WitchCraft server set up, no technical knowledge required!

[![BisectHosting - 25% off your first month using promo code "hate"!](https://i.imgur.com/PRz3yiq.png)](https://bisecthosting.com/hate)

 

Learn magic!
![img](https://media.forgecdn.net/attachments/313/623/library.png)

 

Fight frightening monsters!
![img](https://media.forgecdn.net/attachments/313/622/monsters.png)

 

Travel to wonderous dimensions!
![img](https://media.forgecdn.net/attachments/313/621/portal.png)

 

Explore strange places!
![img](https://media.forgecdn.net/attachments/313/619/tower.png)

Fight through massive dungeons!
![img](https://media.forgecdn.net/attachments/313/618/ziggurat.png)

 

Attain enlightenment!
 ![img](https://media.forgecdn.net/attachments/313/620/sky.png)
