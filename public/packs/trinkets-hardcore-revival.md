---
Title: 'Trinkets Hardcore Revival'
Author: 'Sweet_lil_Trinket'
Description: 'Hardcore Revival is a traditional high fantasy adventure modpack'
Image: 'trinkets-hardcore-revival.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/trinkets-hardcore-revival'
Categories: ['adventure','exploration','magic','quests','hardcore']
---
![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Header.png)[![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fTrinket)

**Ever wanted to to host a server for yourself or your friends? Use code Trinket at checkout for 25% off of your first month of server hosting! [https://bisecthosting.com/Trinket](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fTrinket)**

***This creation of mine is in beta for a reason, I am the only person working on the project so updates will be weekly since im not a professional, just a player with a passion. I have plans to finish the quest book for a more progressive feel but that might take some time, anyone interested in helping should join the discord ^~^.***

**THE GENERAL GIST:**

**Hardcore Revival is my take on the traditional high fantasy adventure modpack and is intended to deliver more of a bite than your average  pack while staying within the reach of the average modpack player, I built the pack from the ground up to add an additional edge to the traditional Minecraft gameplay with new immersive and challenging mechanics that feel rewarding to overcome instead of deliberately cruel. To go along with the Fantasy and RPG themes the pack is stuffed full of magic mods old and new, countless deadly creatures to slay and tame, vast landscapes littered with elaborate dungeons, deep farming and cooking extensions, limitless unique furniture and build auditions, SIX unique Dimensions to explore with massive overhauls to existing ones and encounters to keep even the most seasoned adventurers on their toes. This modpack uses a combination of fundamentally game changing mods that enhance the traditional Minecraft game play by adding thirst, changing the players health and how they take damage, armor weight, decreasing stack size, crop growth, weather and seasons, making mobs smarter and more challenging, realistic terrain, and intense visuals to provide more challenging and immersive game play. **

![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Home.png)**This world is certainly an unforgiving one… But underneath the sickening swarms of monsters and rotting ruins of this ancient place there is an endless beauty in the sprawling landscapes and forests that yearns to be conquered.**

Spoiler (click to show)

Each deeply complex world is the product of an insane combination of the powerful terrain generator Terraforged, the classic beauty of Traverse, a full suite of living forests from Dynamic Trees, Yung’s Host of amazing: Cave, Mineshaft and stronghold revamps, a host of new immersive creatures and unique monsters, powerful new bosses, and endless dungeons and Structures to plunder… if you're brave enough. Possibly my three of my favorite mods; Ice and fire, Mowzies Mobs and Lycanites mobs, provide an absolutely yucky arsenal of mythological creatures to slay and tame when combined with Champions and a Blood-moon. These can range from dragons and sea serpents to cockatrices and Crazy fusion monsters from Lycanites, and are complemented by deep improvements to vanilla mobs and animals with mods like Alex Mobs, Illagers+, Aquaculture, Animalium, Better animals plus, Pams Harvestcraft Food Core, Genetic animals, Rats, Buzzier Bees, Exotic Birds, sapience… You get it there's a lot.

![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Arms.png)**You’re going to need decent gear and strong allies on your side if you want to survive out here, take inventory of the various utilities at your disposal to ensure your backpack and tool belt always have what you need to overcome your foes in those tight spots.**

Spoiler (click to show)

First and foremost your most useful and achievable set of tools are going to be with Silent gear as its the main mod in the pack for customizable gear and is compatible with other mods (Dragon Gear and Managear) for some VERY powerful early and late game options, I recommend starting to use QOL tool mods like Backpacks, Toolbelts, Waystones, Bountiful Baubles, Archers Paradox, Advanced Hook Launchers, Nature's Compass, DWMH, Locks (on the server), Miners Helmet, Torchmaster, training dummies, Iron Furnaces, and a combination of  Iron chest and storage drawers to keep yourself organised . Then surround yourself with powerful allies and pets, Craft an empire with Minecolonies and defend kingdom with guards, train man's best friend to be fearsome pal with Doggy Talents, hire Guard Villagers, Tame  Unique Mounts with Lycanites, Grow a pet dragon with ice and fire and take your world with fire and blood. If you're a builder like me I hope you’ll enjoy the new toys you get to play with in Chisel and bits, Macaws: Doors, Trapdoors and windows, Fairy Lights, Cooking for blockheads, Supplementaries, Decorative Blocks and extended lights to help really bring your world to life.

![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Arcane.png)

**The magics of this place are complex and deep and offer lessons both new and old that are sure to keep you digging through dusty tomes with unbridled curiosity , often integrated into the world ive found everything from the flowers and bees, to the stars in the sky all serve a purpose that is deeper than the surface level.**

Spoiler (click to show)

The magic mods are a plenty in the pack with old favorites like Botania, Astral Sorcery, Evilcraft and Bloodmagic  front rowl alongside a host of new and exciting Mods like Ars Nouveau, Mana and Artifice, Enigmatic Legacy, Eidolon, Lord Craft and Mahou Tsukai. Possibly one of my favorite things about the pack is all the amazing dimensions to explore like Twilight forrest, Atum, The undergarden, The abyss and Ratlantis, Vanilla enchanting is also revolutionised by the combination of Apotheosis and Enscorilation to allow higher than normal levels.

![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Socials.png)

The official public server for the modpack "Haven" is graciously hosted by bisect hosting at the ip:[135.148.51.120:25565](https://www.curseforge.com/linkout?remoteUrl=http://135.148.51.120:25565/)

As for any modpack I highly suggest you give java a solid 4-8 gigs of ram and the addition of optifine although im not allowed to redistribute it here.

For more information, questions, bug reports, or just to say hi come join the discord! I also host a public vanilla server!

Art credits for the artwork are: ClownKing on discord

Vanilla Server ip is: 51.81.110.152:25565

Modlist:

*   Spoiler (click to show)

    *   [Just Enough Resources (JER) (by way2muchnoise)](https://www.curseforge.com/minecraft/mc-mods/just-enough-resources-jer)
    *   [Serene Seasons (by TheAdubbz)](https://www.curseforge.com/minecraft/mc-mods/serene-seasons)
    *   [Illagers+ (by LiteWolf101)](https://www.curseforge.com/minecraft/mc-mods/illagers)
    *   [Inventory Sorter (by cpw)](https://www.curseforge.com/minecraft/mc-mods/inventory-sorter)
    *   [Simple Voice Chat (by henkelmax)](https://www.curseforge.com/minecraft/mc-mods/simple-voice-chat)
    *   [Create (by simibubi)](https://www.curseforge.com/minecraft/mc-mods/create)
    *   [InsaneLib (by Insane96MCP)](https://www.curseforge.com/minecraft/mc-mods/insanelib)
    *   [Netherite Horse Armor Mod (by louie\_cai)](https://www.curseforge.com/minecraft/mc-mods/netherite-horse-armor-mod)
    *   [Locks (by Melonslise)](https://www.curseforge.com/minecraft/mc-mods/locks)
    *   [Atum 2: Return to the Sands (by Shadowclaimer)](https://www.curseforge.com/minecraft/mc-mods/atum)
    *   [Cosmetic Armor Reworked (by LainMI)](https://www.curseforge.com/minecraft/mc-mods/cosmetic-armor-reworked)
    *   [Collective (by Serilum)](https://www.curseforge.com/minecraft/mc-mods/collective)
    *   [PackMenu (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/packmenu)
    *   [Small Ships (by talhanation)](https://www.curseforge.com/minecraft/mc-mods/small-ships)
    *   [Regrowth (by MacTso)](https://www.curseforge.com/minecraft/mc-mods/regrowth)
    *   [Botania (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/botania)
    *   [The Abyss: Chapter II (by y4z0n)](https://www.curseforge.com/minecraft/mc-mods/the-abyss-chapter-ii)
    *   [Pirates And Looters Mod (by abukkezsaruj)](https://www.curseforge.com/minecraft/mc-mods/pirates-and-looters-mod)
    *   [Dragon Gear (by Partonetrain)](https://www.curseforge.com/minecraft/mc-mods/dragon-gear)
    *   [Stoneholm, Underground Villages (Forge) (by TheGrimsey)](https://www.curseforge.com/minecraft/mc-mods/stoneholm-forge)
    *   [FTB Library (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-library-forge)
    *   [Snow! Real Magic! ⛄ (by Snownee\_)](https://www.curseforge.com/minecraft/mc-mods/snow-real-magic)
    *   [FTB Teams (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-teams-forge)
    *   [Enhanced Celestials - Blood Moons & Harvest Moons (by Corgi\_Taco)](https://www.curseforge.com/minecraft/mc-mods/enhanced-celestials)
    *   [Fast Furnace minus Replacement (by tfarecnim)](https://www.curseforge.com/minecraft/mc-mods/fastfurnace-minus-replacement)
    *   [Buzzier Bees (by TeamAbnormals)](https://www.curseforge.com/minecraft/mc-mods/buzzier-bees)
    *   [Kiwi 🥝 (by Snownee\_)](https://www.curseforge.com/minecraft/mc-mods/kiwi)
    *   [CoFH Core (by TeamCoFH)](https://www.curseforge.com/minecraft/mc-mods/cofh-core)
    *   [TerraForged (by won\_ton\_)](https://www.curseforge.com/minecraft/mc-mods/terraforged)
    *   [Abnormals Core (by TeamAbnormals)](https://www.curseforge.com/minecraft/mc-mods/abnormals-core)
    *   [Repurposed Structures (Forge) (by telepathicgrunt)](https://www.curseforge.com/minecraft/mc-mods/repurposed-structures)
    *   [Waystones (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/waystones)
    *   [Comforts (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/comforts)
    *   [Placebo (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/placebo)
    *   [Flywheel (by jozufozu)](https://www.curseforge.com/minecraft/mc-mods/flywheel)
    *   [MythicBotany (by noeppinoeppi)](https://www.curseforge.com/minecraft/mc-mods/mythicbotany)
    *   [iChunUtil (by ohaiiChun)](https://www.curseforge.com/minecraft/mc-mods/ichunutil)
    *   [Farming for Blockheads (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/farming-for-blockheads)
    *   [ItemPhysic Full (by CreativeMD)](https://www.curseforge.com/minecraft/mc-mods/itemphysic)
    *   [Better Advancements (by way2muchnoise)](https://www.curseforge.com/minecraft/mc-mods/better-advancements)
    *   [Tool Belt (by gigaherz)](https://www.curseforge.com/minecraft/mc-mods/tool-belt)
    *   [Silent Lib (silentlib) (by SilentChaos512)](https://www.curseforge.com/minecraft/mc-mods/silent-lib)
    *   [Hats (by ohaiiChun)](https://www.curseforge.com/minecraft/mc-mods/hats)
    *   [Double Doors (by Serilum)](https://www.curseforge.com/minecraft/mc-mods/double-doors)
    *   [MmmMmmMmmMmm (by bonusboni)](https://www.curseforge.com/minecraft/mc-mods/mmmmmmmmmmmm)
    *   [Comfortable Nether (by TheRealZonko)](https://www.curseforge.com/minecraft/mc-mods/comfortable-nether)
    *   [Bookshelf (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/bookshelf)
    *   [Silent Gear (by SilentChaos512)](https://www.curseforge.com/minecraft/mc-mods/silent-gear)
    *   [Infernal Expansion (by InfernalStudios)](https://www.curseforge.com/minecraft/mc-mods/infernal-expansion)
    *   [Lord Craft (by Cobbs\_3)](https://www.curseforge.com/minecraft/mc-mods/lord-craft)
    *   [Eidolon (by elucent\_)](https://www.curseforge.com/minecraft/mc-mods/eidolon)
    *   [Genetic Animals (by mokiyoki)](https://www.curseforge.com/minecraft/mc-mods/genetic-animals)
    *   [Savage & Ravage (by TeamAbnormals)](https://www.curseforge.com/minecraft/mc-mods/savage-and-ravage)
    *   [Structurize (by H3lay)](https://www.curseforge.com/minecraft/mc-mods/structurize)
    *   [Tough As Nails (by TheAdubbz)](https://www.curseforge.com/minecraft/mc-mods/tough-as-nails)
    *   [The Endergetic Expansion (by TeamAbnormals)](https://www.curseforge.com/minecraft/mc-mods/endergetic)
    *   [FTB Ranks (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-ranks-forge)
    *   [YUNG's API (Forge) (by YUNGNICKYOUNG)](https://www.curseforge.com/minecraft/mc-mods/yungs-api)
    *   [FTB Chunks (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-chunks-forge)
    *   [Corpse (by henkelmax)](https://www.curseforge.com/minecraft/mc-mods/corpse)
    *   [Environmental (by TeamAbnormals)](https://www.curseforge.com/minecraft/mc-mods/environmental)
    *   [Ensorcellation (by TeamCoFH)](https://www.curseforge.com/minecraft/mc-mods/ensorcellation)
    *   [Ice and Fire: Dragons (by alex1the1666)](https://www.curseforge.com/minecraft/mc-mods/ice-and-fire-dragons)
    *   [Carry On (by Tschipp)](https://www.curseforge.com/minecraft/mc-mods/carry-on)
    *   [Armor Toughness Bar (by tfarecnim)](https://www.curseforge.com/minecraft/mc-mods/armor-toughness-bar)
    *   [Curious Elytra (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/curious-elytra)
    *   [Camera Mod (by henkelmax)](https://www.curseforge.com/minecraft/mc-mods/camera-mod)
    *   [JEI Integration (by SnowShock35)](https://www.curseforge.com/minecraft/mc-mods/jei-integration)
    *   [Spice of Life: Carrot Edition (by lordcazsius)](https://www.curseforge.com/minecraft/mc-mods/spice-of-life-carrot-edition)
    *   [Better Weather - Seasons, Blizzards, and more! (by Corgi\_Taco)](https://www.curseforge.com/minecraft/mc-mods/better-weather)
    *   [Curios API (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/curios)
    *   [Dungeon Crawl (by XIROC1337)](https://www.curseforge.com/minecraft/mc-mods/dungeon-crawl)
    *   [Tropicraft (by Cojomax99)](https://www.curseforge.com/minecraft/mc-mods/tropicraft)
    *   [Champions (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/champions)
    *   [Crafting Tweaks (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/crafting-tweaks)
    *   [Dynamic Trees + (by supermassimo0310)](https://www.curseforge.com/minecraft/mc-mods/dynamictreesplus)
    *   [When Dungeons Arise (by Aureljz)](https://www.curseforge.com/minecraft/mc-mods/when-dungeons-arise)
    *   [Stalwart Dungeons (by Furti\_Two)](https://www.curseforge.com/minecraft/mc-mods/stalwart-dungeons)
    *   [U Team Core (by HyCraftHD)](https://www.curseforge.com/minecraft/mc-mods/u-team-core)
    *   [Archer's Paradox (by TeamCoFH)](https://www.curseforge.com/minecraft/mc-mods/archers-paradox)
    *   [Dude! Where's my Horse? (by Noobanidus)](https://www.curseforge.com/minecraft/mc-mods/dude-wheres-my-horse)
    *   [AutoRegLib (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/autoreglib)
    *   [Torchmaster (by xalcon)](https://www.curseforge.com/minecraft/mc-mods/torchmaster)
    *   [Architectury API (Forge) (by shedaniel)](https://www.curseforge.com/minecraft/mc-mods/architectury-forge)
    *   [EnhancedVisuals (by CreativeMD)](https://www.curseforge.com/minecraft/mc-mods/enhancedvisuals)
    *   [Xaero's World Map (by xaero96)](https://www.curseforge.com/minecraft/mc-mods/xaeros-world-map)
    *   [AmbientSounds 3 (by CreativeMD)](https://www.curseforge.com/minecraft/mc-mods/ambientsounds)
    *   [Extended Lights (by Polyvalord)](https://www.curseforge.com/minecraft/mc-mods/extended-lights-mod)
    *   [Useful Backpacks (by HyCraftHD)](https://www.curseforge.com/minecraft/mc-mods/useful-backpacks)
    *   [Lycanites Mobs (by Lycanite)](https://www.curseforge.com/minecraft/mc-mods/lycanites-mobs)
    *   [Valhelsia Core (by ValhelsiaTeam)](https://www.curseforge.com/minecraft/mc-mods/valhelsia-core)
    *   [FTB Essentials (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-essentials-forge)
    *   [Controlling (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/controlling)
    *   [Mana and Artifice (by AWildCanadianEh)](https://www.curseforge.com/minecraft/mc-mods/mana-and-artifice)
    *   [Enigmatic Legacy (by Aizistral)](https://www.curseforge.com/minecraft/mc-mods/enigmatic-legacy)
    *   [XP From Harvest (by ReaIGecko)](https://www.curseforge.com/minecraft/mc-mods/xp-from-harvest)
    *   [Blood Magic (by WayofTime)](https://www.curseforge.com/minecraft/mc-mods/blood-magic)
    *   [ForgeEndertech (by EnderLanky)](https://www.curseforge.com/minecraft/mc-mods/forgeendertech)
    *   [YUNG's Better Mineshafts (Forge) (by YUNGNICKYOUNG)](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-forge)
    *   [Nether's Exoticism (by Furti\_Two)](https://www.curseforge.com/minecraft/mc-mods/nethers-exoticism)
    *   [Advanced Hook Launchers (by EnderLanky)](https://www.curseforge.com/minecraft/mc-mods/advanced-hook-launchers)
    *   [Enchantment Descriptions (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/enchantment-descriptions)
    *   [FTB Quests (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-quests-forge)
    *   [Cooking for Blockheads (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/cooking-for-blockheads)
    *   [SwingThroughGrass (by exidex)](https://www.curseforge.com/minecraft/mc-mods/swingthroughgrass)
    *   [AttributeFix (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/attributefix)
    *   [Mana Gear (by Partonetrain)](https://www.curseforge.com/minecraft/mc-mods/mana-gear)
    *   [Craftable Saddles (by alexdaum1)](https://www.curseforge.com/minecraft/mc-mods/craftable-saddles)
    *   [LibX (by noeppinoeppi)](https://www.curseforge.com/minecraft/mc-mods/libx)
    *   [Doggy Talents (by percivalalb)](https://www.curseforge.com/minecraft/mc-mods/doggy-talents)
    *   [Selene (by MehVahdJukaar)](https://www.curseforge.com/minecraft/mc-mods/selene)
    *   [Culinary Construct (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/culinary-construct)
    *   [Kotlin for Forge (by thedarkcolour)](https://www.curseforge.com/minecraft/mc-mods/kotlin-for-forge)
    *   [Giselbaer's Durability Viewer (by Giselbaer)](https://www.curseforge.com/minecraft/mc-mods/giselbaers-durability-viewer)
    *   [Ars Nouveau (by baileyholl2)](https://www.curseforge.com/minecraft/mc-mods/ars-nouveau)
    *   [FastWorkbench (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/fastworkbench)
    *   [AppleSkin (by squeek502)](https://www.curseforge.com/minecraft/mc-mods/appleskin)
    *   [Kobolds! (by Jusey1z)](https://www.curseforge.com/minecraft/mc-mods/kobolds)
    *   [Apotheosis (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/apotheosis)
    *   [Rats: Ratlantis (by alex1the1666)](https://www.curseforge.com/minecraft/mc-mods/rats-ratlantis)
    *   [Guard Villagers (by almightytallestred)](https://www.curseforge.com/minecraft/mc-mods/guard-villagers)
    *   [Iron Furnaces \[FORGE\] (by XenoMustache)](https://www.curseforge.com/minecraft/mc-mods/iron-furnaces)
    *   [The Twilight Forest (by Benimatic)](https://www.curseforge.com/minecraft/mc-mods/the-twilight-forest)
    *   [Just Enough Items (JEI) (by mezz)](https://www.curseforge.com/minecraft/mc-mods/jei)
    *   [Progressive Bosses (by Insane96MCP)](https://www.curseforge.com/minecraft/mc-mods/progressive-bosses)
    *   [Iron Chests (by ProgWML6)](https://www.curseforge.com/minecraft/mc-mods/iron-chests)
    *   [Cloth Config API (Forge) (by shedaniel)](https://www.curseforge.com/minecraft/mc-mods/cloth-config-forge)
    *   [Macaw's Windows (by sketch\_macaw)](https://www.curseforge.com/minecraft/mc-mods/macaws-windows)
    *   [Mowzie's Mobs (by bobmowzie)](https://www.curseforge.com/minecraft/mc-mods/mowzies-mobs)
    *   [Traverse Reforged (by Noobanidus)](https://www.curseforge.com/minecraft/mc-mods/traverse-reforged)
    *   [Caelus API (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/caelus)
    *   [Overloaded Armor Bar (by tfarecnim)](https://www.curseforge.com/minecraft/mc-mods/overloaded-armor-bar)
    *   [Towers Of The Wild (by idrae\_)](https://www.curseforge.com/minecraft/mc-mods/towers-of-the-wild)
    *   [Dynamic Trees - TerraForged (by HarleyOConnor)](https://www.curseforge.com/minecraft/mc-mods/dynamic-trees-terraforged)
    *   [ReAuth (by TechnicianLP)](https://www.curseforge.com/minecraft/mc-mods/reauth)
    *   [Patchouli (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/patchouli)
    *   [MineColonies (by H3lay)](https://www.curseforge.com/minecraft/mc-mods/minecolonies)
    *   [Aquaculture 2 (by Shadowclaimer)](https://www.curseforge.com/minecraft/mc-mods/aquaculture)
    *   [Adventures Structures (by erixon\_07)](https://www.curseforge.com/minecraft/mc-mods/adventures-structures)
    *   [Macaw's Doors (by sketch\_macaw)](https://www.curseforge.com/minecraft/mc-mods/macaws-doors)
    *   [Dynamic Trees - Traverse (by HarleyOConnor)](https://www.curseforge.com/minecraft/mc-mods/dynamic-trees-traverse)
    *   [Performant (by someaddon)](https://www.curseforge.com/minecraft/mc-mods/performant)
    *   [Macaw's Trapdoors (by sketch\_macaw)](https://www.curseforge.com/minecraft/mc-mods/macaws-trapdoors)
    *   [Just Enough Professions (JEP) (by Mrbysco)](https://www.curseforge.com/minecraft/mc-mods/just-enough-professions-jep)
    *   [Alex's Mobs (by alex1the1666)](https://www.curseforge.com/minecraft/mc-mods/alexs-mobs)
    *   [ObserverLib (by HellFirePvP)](https://www.curseforge.com/minecraft/mc-mods/observerlib)
    *   [Pam's HarvestCraft 2 - Food Core (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-food-core)
    *   [Supplementaries (by MehVahdJukaar)](https://www.curseforge.com/minecraft/mc-mods/supplementaries)
    *   [Elenai Dodge (by ElenaiDev)](https://www.curseforge.com/minecraft/mc-mods/elenai-dodge)
    *   [Shulker Tooltip (by ZephaniahNoah)](https://www.curseforge.com/minecraft/mc-mods/shulker-tooltip)
    *   [Valhelsia Structures (by ValhelsiaTeam)](https://www.curseforge.com/minecraft/mc-mods/valhelsia-structures)
    *   [Decorative Blocks (by stohun)](https://www.curseforge.com/minecraft/mc-mods/decorative-blocks)
    *   [CreativeCore (by CreativeMD)](https://www.curseforge.com/minecraft/mc-mods/creativecore)
    *   [YUNG's Better Strongholds (Forge) (by YUNGNICKYOUNG)](https://www.curseforge.com/minecraft/mc-mods/yungs-better-strongholds)
    *   [Toast Control (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/toast-control)
    *   [Miner's Helmet (by ModdingLegacy)](https://www.curseforge.com/minecraft/mc-mods/miners-helmet)
    *   [Akashic Tome (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/akashic-tome)
    *   [Cyclops Core (by kroeser)](https://www.curseforge.com/minecraft/mc-mods/cyclops-core)
    *   [Mouse Tweaks (by YaLTeR)](https://www.curseforge.com/minecraft/mc-mods/mouse-tweaks)
    *   [Upgrade Aquatic (by TeamAbnormals)](https://www.curseforge.com/minecraft/mc-mods/upgrade-aquatic)
    *   [Storage Drawers (by Texelsaur)](https://www.curseforge.com/minecraft/mc-mods/storage-drawers)
    *   [Dynamic Surroundings (by OreCruncher)](https://www.curseforge.com/minecraft/mc-mods/dynamic-surroundings)
    *   [Chisels & Bits (by AlgorithmX2)](https://www.curseforge.com/minecraft/mc-mods/chisels-bits)
    *   [Dynamic Trees (by ferreusveritas)](https://www.curseforge.com/minecraft/mc-mods/dynamictrees)
    *   [Curio of Undying (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/curio-of-undying)
    *   [GeckoLib (by ThanosGecko)](https://www.curseforge.com/minecraft/mc-mods/geckolib)
    *   [BountifulBaubles (by Cursed1nferno)](https://www.curseforge.com/minecraft/mc-mods/bountifulbaubles)
    *   [FramedBlocks (by XFactHD)](https://www.curseforge.com/minecraft/mc-mods/framedblocks)
    *   [Crafting Station (by tfarecnim)](https://www.curseforge.com/minecraft/mc-mods/crafting-station)
    *   [Tumbleweed (by konwboj)](https://www.curseforge.com/minecraft/mc-mods/tumbleweed)
    *   [Curious Shulker Boxes (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/curious-shulker-boxes)
    *   [IguanaTweaks Reborn (by Insane96MCP)](https://www.curseforge.com/minecraft/mc-mods/iguanatweaks-reborn)
    *   [Enchanted Book Redesign (by tfarecnim)](https://www.curseforge.com/minecraft/mc-mods/enchanted-book-redesign)
    *   [Fairy Lights (by pau101)](https://www.curseforge.com/minecraft/mc-mods/fairy-lights)
    *   [Xaero's Minimap (by xaero96)](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap)
    *   [Citadel (by alex1the1666)](https://www.curseforge.com/minecraft/mc-mods/citadel)
    *   [TrashSlot (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/trashslot)
    *   [Seals (by Buuz135)](https://www.curseforge.com/minecraft/mc-mods/seals)
    *   [Dynamic Trees - Atum 2 (by supermassimo0310)](https://www.curseforge.com/minecraft/mc-mods/dynamic-trees-atum-2)
    *   [Nature's Compass (by Chaosyr)](https://www.curseforge.com/minecraft/mc-mods/natures-compass)
    *   [EvilCraft (by kroeser)](https://www.curseforge.com/minecraft/mc-mods/evilcraft)
    *   [The Undergarden (by Quek04)](https://www.curseforge.com/minecraft/mc-mods/the-undergarden)
    *   [Neat (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/neat)
    *   [Exotic Birds (by ThePavoReality)](https://www.curseforge.com/minecraft/mc-mods/exotic-birds)
    *   [Rats (by alex1the1666)](https://www.curseforge.com/minecraft/mc-mods/rats)
    *   [Animalium (by vadis365)](https://www.curseforge.com/minecraft/mc-mods/animalium)
    *   [Goblin Traders (by MrCrayfish)](https://www.curseforge.com/minecraft/mc-mods/goblin-traders)
    *   [Item Filters (Forge) (by LatvianModder)](https://www.curseforge.com/minecraft/mc-mods/item-filters-forge)
    *   [Astral Sorcery (by HellFirePvP)](https://www.curseforge.com/minecraft/mc-mods/astral-sorcery)
    *   [Jade 🔍 (by Snownee\_)](https://www.curseforge.com/minecraft/mc-mods/jade)
    *   [Extractures (by itsminiLink)](https://www.curseforge.com/minecraft/mc-mods/extractures)
    *   [Mahou Tsukai (by stepcros)](https://www.curseforge.com/minecraft/mc-mods/mahou-tsukai)
    *   [Spiders 2.0 (by TheCyberBrick)](https://www.curseforge.com/minecraft/mc-mods/spiders-2-0)
    *   [Sapience (by the\_infamous\_1)](https://www.curseforge.com/minecraft/mc-mods/sapience)
    *   [Better Animals Plus (by cybercat5555)](https://www.curseforge.com/minecraft/mc-mods/betteranimalsplus)

[![](https://cdn.discordapp.com/attachments/862061100933972038/862061705251782676/1ah07M7.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)

Not your speed check out some of my other projects: [https://www.curseforge.com/members/sweet\_lil\_trinket/projects](https://www.curseforge.com/members/sweet_lil_trinket/projects)

[![](https://www.bisecthosting.com/images/CF/TrinketsEngineers/BH_TH_Header.png)](https://www.curseforge.com/minecraft/modpacks/trinkets-hardcore-revival-engineers-edition)[![](https://www.bisecthosting.com/images/CF/Blood_&_Stone/BH_BS_HEADER.png)](https://www.curseforge.com/minecraft/modpacks/blood-and-stone)

[![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Discord.png)](https://discord.gg/W39TBkek9A)

[![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Twitch.png)![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Reddit.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fuser%252fnotTrinket)[![](https://www.bisecthosting.com/images/CF/Trinkets/BH_TH_Twitter.png)](https://twitter.com/Trinket_Pettle?s=09)
