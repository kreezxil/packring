---
Title: 'BioSphere War'
Author: 'LWLegends'
Description: 'Sky Bioshperes filled with Post Apocalyptic cities'
Image: 'biosphere-war.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/biosphere-war'
Categories: ['skyblock', 'adventure', 'combat', 'quests', 'multiplayer']
---
![](https://media.forgecdn.net/attachments/378/622/lsmpatreonbanner1.png)

***A new type of Skyblock!***

**100's of Custom Quests and Rewards**, many of which can be considered very OP.  This is intentional so that you may focus on building your military might in order to crush your enemies without having to endure hours of grinding for resources.  With that being said, don't expect this pack to be super easy... **You WILL Die**... a lot... at least in the beginning days of you getting settled in and set up your base.

Have you ever played a modpack and finished the quest book and then said "What now?"  Well, with this modpack, finishing the quest book is just the beginning!  Now you can focus on clearing your Biosphere Dome and creating a Full-Blown Military Base complete with Tanks, Jets, Helicopters, Anti-Aircraft guns and full armories so that you can attack or defend as you see fit!

For those who are not into tanks, helicopters, airplanes, and jets, don't worry.  This modpack is just as fun filled while playing it as a Sky World Questing Modpack without ever getting into the Flans portion of the pack.

***They did it... they finally did it... they pushed the "button"."***

It all started with the new technology they invented allowing for true gravitational levitation. At first, things were going great, it was the rage everywhere... levitating vehicles, personal levitation, and even homes floating in mid air.  That was our downfall... the floating home.  Due to this technology, the wealthy 1% started losing money at an alarming rate as their property values plummeted.  It became a nightmare in the courts, then all the way to Congress and the White House.  Soon the financial meltdown had reached around the world as banks went out of business and governments who were once allies began to argue and fight among each other.  With no solution in sight it wasn't long before the world felt the embrace of war once again... this time it ended very badly.  No one knows now who actually launched the first Nuclear missile, but I guess it really doesn't matter now.  I say it ended.. well, actually its still going on. But instead of governments, it's now factions and groups of survivors fighting from the last levitating Biospheres for the few scraps of resources left available.

Now, over 100 years since the bombings, our civilization has become a world of floating biodomes surrounded by nothing but void. Many of these biospheres contain the remains of war ravaged and abandoned cities and towns, while others lay barren with no signs of life... well, human life anyway.  The fallout from the radiation has had some, well, let's just say, unexpected side effects on the majority of the survivors. Can you and your allies survive to rebuild a military to defend yourselves... or to attack other survivors for their hard earned resources?

I will consistently be updating this pack based on your comments here.  If you have a suggestion or concern, please let me know.  Enjoy!

***![](https://media.forgecdn.net/attachments/378/597/biospherewar.png)***

***PUBLIC TEST SERVER IP:  155.94.181.188:25565 ***

**Come join the fun!**

[**![](https://media.forgecdn.net/attachments/362/29/discordlogo1.png)**](https://discord.gg/bexuaUpkUx)

[**![](https://www.bisecthosting.com/partners/custom-banners/4f585742-43a0-42fb-bb7f-a5f032175ee9.png)**](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252flegends)

[![](https://media.forgecdn.net/attachments/362/238/patronbutton.png)](https://www.patreon.com/bePatron?u=55271169)

[![](https://media.forgecdn.net/attachments/thumbnails/378/596/310/172/1ah07m7.png)](https://www.curseforge.com/linkout?remoteUrl=https://hate9.gitlab.io/packring/)

***Third Party Mod License Appreciation***

Special thanks to the Flans Mod creators for allowing us to use their mod in this pack:  [https://flansmod.com/license](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fflansmod.com%252flicense)

***Modlist***

*   [Item Filters (by LatvianModder)](https://minecraft.curseforge.com/mc-mods/309674)
*   [Actually Additions (by Ellpeck)](https://minecraft.curseforge.com/mc-mods/228404)
*   [Custom Starter Gear (by brandon3055)](https://minecraft.curseforge.com/mc-mods/253735)
*   [CraftTweaker (by Jaredlll08)](https://minecraft.curseforge.com/mc-mods/239197)
*   [CodeChicken Lib 1.8.+ (by covers1624)](https://minecraft.curseforge.com/mc-mods/242818)
*   [AppleSkin (by squeek502)](https://minecraft.curseforge.com/mc-mods/248787)
*   [Thermal Cultivation (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/271835)
*   [ProjectE (by SinKillerJ)](https://minecraft.curseforge.com/mc-mods/226410)
*   [Better Builder's Wands (by Portablejim)](https://minecraft.curseforge.com/mc-mods/238403)
*   [Iron Chests (by ProgWML6)](https://minecraft.curseforge.com/mc-mods/228756)
*   [CoFH Core (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/69162)
*   [Thermal Innovation (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/291737)
*   [The Lost Cities (by McJty)](https://minecraft.curseforge.com/mc-mods/269024)
*   [Thermal Expansion (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/69163)
*   [Pam's HarvestCraft (by pamharvestcraft)](https://minecraft.curseforge.com/mc-mods/221857)
*   [Expanded Equivalence (by Zeitheron)](https://minecraft.curseforge.com/mc-mods/295222)
*   [LootBags (by Malorolam)](https://minecraft.curseforge.com/mc-mods/225946)
*   [XP From Harvest (by ReaIGecko)](https://minecraft.curseforge.com/mc-mods/289397)
*   [Project EX (by LatvianModder)](https://minecraft.curseforge.com/mc-mods/311378)
*   [Brandon's Core (by brandon3055)](https://minecraft.curseforge.com/mc-mods/231382)
*   [GraveStone Mod (by henkelmax)](https://minecraft.curseforge.com/mc-mods/238551)
*   [CoFH World (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/271384)
*   [Hammer (Lib) Core (by Zeitheron)](https://minecraft.curseforge.com/mc-mods/247401)
*   [Just Enough Items (JEI) (by mezz)](https://minecraft.curseforge.com/mc-mods/238222)
*   [Thermal Foundation (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/222880)
*   [Inventory Pets (by Purplicious\_Cow\_)](https://minecraft.curseforge.com/mc-mods/229380)
*   [FTB Quests (by LatvianModder)](https://minecraft.curseforge.com/mc-mods/289412)
*   [Extra Utilities (by RWTema)](https://minecraft.curseforge.com/mc-mods/225561)
*   [Redstone Flux (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/270789)
*   [Mob Grinding Utils (by vadis365)](https://minecraft.curseforge.com/mc-mods/254241)
*   [Thermal Dynamics (by TeamCoFH)](https://minecraft.curseforge.com/mc-mods/227443)
*   [JourneyMap (by techbrew)](https://minecraft.curseforge.com/mc-mods/32274)
*   [EnderCore (by tterrag1098)](https://minecraft.curseforge.com/mc-mods/231868)
*   [Ore Excavation (by Funwayguy)](https://minecraft.curseforge.com/mc-mods/250898)
*   [Inventory Tweaks (by Kobata)](https://minecraft.curseforge.com/mc-mods/223094)
*   [Draconic Evolution (by brandon3055)](https://minecraft.curseforge.com/mc-mods/223565)
*   [Traveller's Backpack (by tiviacz1337)](https://minecraft.curseforge.com/mc-mods/321117)
*   [Chest Transporter (by cubex2)](https://minecraft.curseforge.com/mc-mods/78778)
*   [LootTweaker (by Daomephsta)](https://minecraft.curseforge.com/mc-mods/255257)
*   [FTB Library (by LatvianModder)](https://minecraft.curseforge.com/mc-mods/237167)
*   [Hwyla (by TehNut)](https://minecraft.curseforge.com/mc-mods/253449)
*   [Mouse Tweaks (by YaLTeR)](https://minecraft.curseforge.com/mc-mods/60089)
*   [Ender IO (by CrazyPants\_MC)](https://minecraft.curseforge.com/mc-mods/64578)
*   [CompactStorage (by tobyystrong)](https://minecraft.curseforge.com/mc-mods/223703)
*   [JEI Integration (by SnowShock35)](https://minecraft.curseforge.com/mc-mods/265917)
*   [BetterFps (by Guichaguri)](https://minecraft.curseforge.com/mc-mods/229876)
