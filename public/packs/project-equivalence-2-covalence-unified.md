---
Title: 'Project Equivalence 2 Covalence Unified'
Author: 'thenimbleninja'
Description: 'The sequel to project equivalence!'
Image: 'project-equivalence-2-covalence-unified.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/project-equivalence-2-covalence-unified'
Categories: ['skyblock','magic','quests','tech']
---
![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Header.png)

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Overview.png)

Welcome to the sequel to project equivalence! Project Equivalence 2 Covalence Unified is an even bigger and harder take on the first modpack. Just like the first pack you start in a skyblock, but unlike the first one you don't get all resources with emc straight away! You have to use my custom scavenge recipes to excavate and sieve resources out of dirt and stones!

Just like the first pack the goal of the pack is to make the final star its going to be a tough journey to make it but in the end it will be all worth it! In this pack you don't start with a watch ore philosopher's stone you have to create them yourself!

What's special in this version is an overworld challenge preset you wont start out with the emc table so you will have to work towards it! Lycanites mobs and gai mobs will make the over world extremely deadly to live in have fun :)

The pack will have about the same presets as the first pack with the over world being the challenge world! This time quests will have money for rewards and there will be more quests to complete with separate categories for each quest tier. The money will be used in a shop block to buy rewards! You can also sell some resources to gain money other than quests!

But other than that, my goal is to make an expert pack that is fun to play i hope you all have fun playing it :)

 ![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Community.png)

[![](https://i.imgur.com/4k8lKdV.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.guilded.gg%252fi%252fRkrdW7lE)[![](https://i.imgur.com/uabSTGk.png)![](https://cdn.discordapp.com/attachments/862061100933972038/865086703261974598/packring-logo.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)

                                             Project Equivalence has partnered with BisectHosting!
[![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fTheNimbleNinja)

**BisectHosting** offers quick, simple, and high-quality server hosting with over a dozen locations available across the world, ensuring low                                            latency. After your purchase, the server is already configured and ready to use.

                Use the code **"TheNimbleNinja"** to get **25% OFF** your first month as a new client for any of their gaming servers.
