---
Title: 'The Cube Reborn'
Author: 'crashnebula240'
Description: 'Take your weapons against those pesky gods who have wronged you'
Image: 'the-cube-reborn.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/the-cube-reborn'
Categories: ['pvp','exploration','multiplayer','tech','large']
---
![](https://www.bisecthosting.com/images/CF/The_Cube_Reborn/BH_TCR_LOGO.png)

***Are you ready to face the gods wrath?***

The Cube:Reborn is a carefully crafted expert modpack, with the aim of enhancing the player experience to levels not seen before.

We have carefully crafted this modpack with my team in order to not only being fun, but also being unique and not performance intensive.

*Enjoy playing with 200+ mods with only 2 GB of ram! Bye bye lag!

*A big update is on the way!! The TOTAL OVERHAUL UPDATE - 4.0!!
More info on our discord server!!

*Defy the gods:
**Take your weapons against those pesky gods who have wronged you, use relics, magical artifacts, your own abilities and training to fight them. Bring a friend or summon powerful beings to fight next to your side, find gear to slaughter mighty foes in the many ruins left behind, make your own story, become legend.**

**Main features!*

*   *Made to run on low end PC's!*
*   *Has an official server with plugins!*
*   *Quests to guide the player!*
*   *New recipes!*
*   *QoL mods to enhance the experience!*

 ![](https://i.postimg.cc/FzHTxBbw/overview.png)

**The Cube: Reborn has a little for everyone!**

*   Are you a **builder**?? Well, lots of new blocks specifically made for all of you builders out there to make wonderful structures with!! Also with **Effortless Building**, building big structures is no biggie! Also with **Secret Rooms **you can camouflage your base! Making it virtually invisible to other players!! Like building bridges? Well, **Macaw's Bridges** got you covered!! Never liked Minecraft glass panes? Well with **Macaw's Windows** you can create better buildings!! Also there's **Blockcarpentry**, which allows you to use almost every block you desire to be used on different shapes!!

*   Are you a lover of **tech **mods?? Well with **Create** you can now make immersive buildings and contraptions to make your life easier!! Automate all of your task, the only limit its your imagination.

*   Are you an **adventurer**?? Oh boy, let me tell you, you have ***a lot*** to explore, from small ruins across the world to entire castles filled with monsters and treasures!! If you are not satisfied with the Overworld, you can go and explore more than 20+ different action packed dimensions thanks to **Advent of Ascension**, each one with new foes and rewards to find, and specifically tweaked with new structures completely made for the modpack!! Or go through the many new dungeons that **Dungeons Arise **and **Repurposed Structures** adds to your world!!

*   Are you a **farmer**?? You like enjoying a nice view of your crops, in a nice farm just chilling out?? Well with a lot of crops being added and with more cozy decorations you will be able to do that...and more!! With mods such as **Farmer's Delight **your farm is guaranteed to be a success!!

*   Are you a **collectionist**?? Can't sleep if you can't have nice trophies of your fallen foes? Can't rest if you can't get this players head? Well wait no more! There are now trophies!! These neat little blocks are avaliable for almost every mob, and will drop when you kill them many times! And if you play in multiplayer, killing players has a chance to drop their head! Also thanks to **Artifacts, Relicex **and **Relics** killing mobs is a rewarding activity, more than ever.

*   Are you a **fisherman**?? Well, not many understand the life of a fisherman, is a quiet and peaceful life... Well, now you can finally get rewarded for your patience! As fishing is now a viable way to get new and exclusive loot thanks to **Upgrade Aquatic** and **Aquaculture 2**, with more fish and even fish weights!(For you who like to be competitive 😉)

**Join our Discord!! Link: [Our Discord!!](https://discord.gg/x8GshCvAHS)**

**[![](https://img.shields.io/discord/802392609842659350.svg?color=7289DA&label=discord&logo=discord&logoColor=FFFFFF&style=flat-square)](https://discord.gg/x8GshCvAHS)**

Those are just small amount of all the features that the modpack includes! Features are down here!!

![](https://i.postimg.cc/nc53vTXw/features.png)

Spoiler (click to show)

*   More than 20 dimensions to explore!!
*   Reworked and completely new Advent of Ascension structures!! (**Done by me!! :D)**
*   **Scaffolding drops near you!**
*   Custom sprites for enchanted books! (**Advent of Ascension books made by me and Witherlord, and Alex mobs books done by me!!)**
*   Start your adventure with new items!!
*   Better hitboxes for Rabbits and Silverfish!!
*   Discover artifacts and relics to help you in your adventure!
*   Better caves!!
*   More structures and improved vanilla structures!!
*   A lot of new enchantments!! Each of them with new sprites!!
*   New mobs and bosses to fight!!
*   More boats!
*   Useful minecarts and rails!!
*   Create hidden bases with secret blocks!
*   Travel around the world with waystones!
*   New netherite tools and armour!
*   More endgame loot!
*   New custom recipes (and more to come!)
*   Create different automations using Create!
*   More and reworked shields!! Now shields don't block all damage incoming.
*   Get player heads by killing other players!!
*   New player progression system! Use skill points to level up your character abilities!!
*   Reworked crits and damage mechanics!
*   Experience the 1.17 update in 1.16!!
*   **And more and more to discover!!**

![](https://i.postimg.cc/G2r7424L/IMAGES.png)

Spoiler (click to show)

Here's a list of pictures:

**You can hear the flesh moving around...
![](https://i.postimg.cc/KjZ93MbZ/2021-05-28-11-41-33.png)****You can see the tower on the verge of falling down...**

**![](https://i.postimg.cc/nhL6wN6H/2021-05-28-11-27-36.png)**

**A knight stands in your way.
![](https://i.postimg.cc/mkXScpVw/2021-05-28-11-42-01.png)What lies in the depths?
![](https://i.postimg.cc/Y0KH58Qd/2021-05-28-11-24-52.png)The oceans are wonderful and dangerous places...
![](https://i.postimg.cc/X76jmN8n/2021-05-28-11-23-39.png)Retextured Nether Crystals.
![](https://i.postimg.cc/PJ2SW5f1/2021-05-28-11-39-21.png)An old ship awaits...
![](https://i.postimg.cc/8zqr0gjW/2021-05-28-01-31-39.png)Shiny places!
![](https://i.postimg.cc/sxcpk6tz/2021-05-28-01-21-41.png)Old aventurer loot deep inside in caves.
![](https://i.postimg.cc/wTwXFrXc/2021-05-28-01-26-08.png)What is this undead treehouse?
![](https://i.postimg.cc/445VN0Sk/2021-05-28-01-27-09.png)Long gone beasts remains in the Overworld.
![](https://i.postimg.cc/BntxzCNW/2021-05-28-01-26-40.png)Giants roam the land...
![](https://i.postimg.cc/2SqZGS8r/2021-05-28-11-22-15.png)Don't get lost!
![](https://i.postimg.cc/LXVY5qvB/2021-05-28-11-21-06.png)I can't imagine what kind of bees polinate this flowers...
![](https://i.postimg.cc/zXh77NDm/2021-05-28-16-08-32.png)Resting in a field of flowers after a long adventure day...
![](https://i.postimg.cc/RZQNXJxr/2021-05-28-13-35-49.png)Lush caves!!
![](https://i.postimg.cc/RVt3JbPZ/2021-05-28-16-05-21.png)Jurassic Park?
![](https://i.postimg.cc/G2RK6yHk/2021-05-28-11-43-27.png)Pillagers camping in the wilderness:
![](https://i.postimg.cc/VsBB0v5h/2021-05-28-01-25-05.png)The Barakoa chiefs...
![](https://i.postimg.cc/gk2RwKCw/2021-05-28-01-28-13.png)Create amazing stuff like "Honeyland" using Create!
![](https://i.postimg.cc/ncRcNsFY/2021-09-08-00-46-09.png)
**

![](https://i.postimg.cc/C5rmrsm5/COMMJNITY.png)If you are looking forward to see more about our community consider following us on our social media!!

 [](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.instagram.com%252fthecube.cl%252f)[![](https://fatimamartinez.es/wp-content/uploads/2018/09/Instagram-logo-de-600.jpg)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.instagram.com%252fthecubereborn%252f)

[![](https://i2.wp.com/hipertextual.com/wp-content/uploads/2012/06/twitter-bird-white-on-blue.jpg?fit=300,300&ssl=1)](https://twitter.com/thecubereborn)

*   The modpack was built around Optifine compatibility! Download it here!
    (Take in mind Optifine slows loading time!! Without Optifine TCR loads in about 5 mins!!)
    [![](https://pbs.twimg.com/profile_images/1336182241613938689/MabYhp8e_400x400.jpg)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252foptifine.net%252fadloadx%253ff%253dOptiFine_1.16.5_HD_U_G8.jar%2526x%253dceee)
    **Are you looking to host your own The Cube Reborn server? Try out Bisect Hosting!**
    They offer amazing hosting services and also, if you use our code "thecube" you will get 25% off your first month and you will be helping us to keep working on the server and you will also get some freebies in our server!!
    [![](https://i.postimg.cc/V6R7n8tj/PROMO.png)![](https://i.postimg.cc/Dz4VLJb8/BH-TCR-CREDITS.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252fthecube)**I owe all of this progress to my long time supporters and friends:**

*   **Skid** - For donating over a 100$ to help with server costs for more than a year
*   **Tsutara** - For being that person who kept me going and supported me and made this love for modpacks grow
*   **Bemo** - For being a supporter despite not having much time to play
*   **Pekke** - For being an amazing guy overall and supporting the server
*   **Alanpros & Santy** - For being the originals supporting and preventing the server death due to lack of funds
*   **Jesusisagoose & Pantsu** - For donating to the modpack when getting to the main menu (love you guys)
*   **Martin\_lilzblit** \- For supporting me, the server and building an amazing lobby area
*   **Srkpedo** - For giving me the opportunity to start this whole thing, even though we have parted ways
*   **Hidrocarburo** - Who made me realize the potential of this modpack and for lending me a big hand
*   **Kenneth** - For making beautiful banners and a lovely logo
*   **To the bisecthosting staff for being kind and giving me an amazing opportunity

    Thanks everyone for supporting me and making this community nice and comfy to stay in!**
