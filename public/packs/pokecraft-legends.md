---
Title: 'PokeCraft Legends'
Author: 'LWLegends'
Description: 'Experience never before seen GameShark Style play!'
Image: 'pokecraft-legends.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/pokecraft-legends'
Categories: ['adventure','exploration','multiplayer','quests']
---
## ***![](https://media.forgecdn.net/attachments/378/749/lsmpatreonbanner2.png)***

# ***POKECRAFT LEGENDS***

Tired of the same old Poke / Pixelmon Packs?

Join us and Experience an ALL NEW Pack featuring Quests, Rewards, Dimensions, Highly edited Configs and Custom Recipes!

Experience never before seen GameShark Style play! Travel to Strange New Dimensions and Capture your old Favorites and many New Pokes!

***Very Special Thanks for the GameShark Mod:   [https://pixelmonmod.com/mirror/sidemods/gameshark/6.0.4/](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpixelmonmod.com%252fmirror%252fsidemods%252fgameshark%252f6.0.4%252f)***

[![](https://media.forgecdn.net/attachments/362/238/patronbutton.png)](https://www.patreon.com/bePatron?u=55271169)

***PUBLIC TEST SERVER IP: 167.114.74.177:25582***

 ![](https://media.forgecdn.net/attachments/378/750/pclserverbanner.png)

**Come join the fun!**

[**![](https://media.forgecdn.net/attachments/362/29/discordlogo1.png)**](https://discord.gg/bexuaUpkUx)

[**![](https://www.bisecthosting.com/partners/custom-banners/4f585742-43a0-42fb-bb7f-a5f032175ee9.png)**](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252flegends)

**[![](https://media.forgecdn.net/attachments/378/596/1ah07m7.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fhate9.gitlab.io%252fpackring%252f)**

***MODLIST***

*   [JEI Integration (by SnowShock35)](https://www.curseforge.com/minecraft/mc-mods/jei-integration)
*   [Custom Starter Gear (by brandon3055)](https://www.curseforge.com/minecraft/mc-mods/custom-starter-gear)
*   [FTB Library (Forge) (Legacy) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-library-legacy-forge)
*   [CraftPresence (by CDAGaming\_)](https://www.curseforge.com/minecraft/mc-mods/craftpresence)
*   [GraveStone Mod (by henkelmax)](https://www.curseforge.com/minecraft/mc-mods/gravestone-mod)
*   [PokehaanCraft Additions (by LCLC98)](https://www.curseforge.com/minecraft/mc-mods/pokehaancraft-additions)
*   [Ore Excavation (by Funwayguy)](https://www.curseforge.com/minecraft/mc-mods/ore-excavation)
*   [The Wild Area for Pixelmon Reforged (by marcobsidiandev)](https://www.curseforge.com/minecraft/mc-mods/the-wild-area-for-pixelmon-reforged)
*   [Inventory Tweaks \[1.12 only\] (by JimeoWan)](https://www.curseforge.com/minecraft/mc-mods/inventory-tweaks)
*   [Just Enough Resources (JER) (by way2muchnoise)](https://www.curseforge.com/minecraft/mc-mods/just-enough-resources-jer)
*   [FTB Quests (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-quests-forge)
*   [Pixelmon Information for Pixelmon Reforged (by Bletch1971)](https://www.curseforge.com/minecraft/mc-mods/pixelmon-information)
*   [JourneyMap (by techbrew)](https://www.curseforge.com/minecraft/mc-mods/journeymap)
*   [Mouse Tweaks (by YaLTeR)](https://www.curseforge.com/minecraft/mc-mods/mouse-tweaks)
*   [FusionPixelmon (by fusiondev\_)](https://www.curseforge.com/minecraft/mc-mods/fusionpixelmon)
*   [Tinkers Construct (by mDiyo)](https://www.curseforge.com/minecraft/mc-mods/tinkers-construct)
*   [Controlling (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/controlling)
*   [Iron Chests (by ProgWML6)](https://www.curseforge.com/minecraft/mc-mods/iron-chests)
*   [MineTweaker RecipeMaker (by DoubleDoorDev)](https://www.curseforge.com/minecraft/mc-mods/minetweaker-recipemaker)
*   [Tinkers' Tool Leveling (by bonusboni)](https://www.curseforge.com/minecraft/mc-mods/tinkers-tool-leveling)
*   [Tinker's JEI (by Possible\_triangle)](https://www.curseforge.com/minecraft/mc-mods/tinkers-jei)
*   [Waystones (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/waystones)
*   [Just Enough Items (JEI) (by mezz)](https://www.curseforge.com/minecraft/mc-mods/jei)
*   [Mantle (by mDiyo)](https://www.curseforge.com/minecraft/mc-mods/mantle)
*   [Traveler's Backpack (by Tiviacz1337)](https://www.curseforge.com/minecraft/mc-mods/travelers-backpack)
*   [Useful Slime (by MincraftEinstein)](https://www.curseforge.com/minecraft/mc-mods/useful-slime)
*   [CraftTweaker (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/crafttweaker)
*   [Pixelmon (by PixelmonMod)](https://www.curseforge.com/minecraft/mc-mods/pixelmon)
*   [Hwyla (by ](https://www.curseforge.com/minecraft/mc-mods/hwyla)
