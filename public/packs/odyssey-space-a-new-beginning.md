---
Title: 'Odyssey: Space - A New Beginning'
Author: 'jelaw21'
Description: 'You, the Commander of the Odyssey, ...'
Image: 'odyssey-space-a-new-beginning.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/odyssey-space-a-new-beginning'
Categories: ['adventure','exploration','pvp','quests','tech']
---

> [*![](https://www.bisecthosting.com/images/CF/Odyssey_Space/BH_OS_PromoCard.png)*](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fdwarfhavengames)
>
> ***Odyssey - a long and eventful or adventurous journey.*  **

"This is the first time I've genuinely been able to start a minecraft run on an actual alien world with the intent of trying to get off of it. This pack will be in my library forever, mate." - Lakmanyen, Discord.

NOW WITH A FULLY COMPLETED FTB QUEST BOOK!!

![](https://www.bisecthosting.com/images/CF/Odyssey_Space/BH_OS_Story.png)

You, the Commander of the Odyssey, have crash landed on a strange alien planet. You must use your resources and the materials you can find to first build a spacecraft, then fly it across the galaxy to find your crewmates and return back home to Overworld.

![](https://www.bisecthosting.com/images/CF/Odyssey_Space/BH_OS_Adventure.png)

But surviving Blue Skies Everdawn and Everbright is your first priority. Then, using Resourceful Bees and some Mystical Agriculture, you will turn the alien materials into resources you need. Discover the secrets of Neon Energy, provided by Futurepack, and use its properties to unlock monorails, drones and space flight. Keep your ship running smoothly with Sophisticated Backpacks, RFToolsand Applied Energistics, supported with Mekanism machines and gases.

If that's not enough, automate everything with all addons of Integrated Dynamics and begin crafting armors, swords, greatswords, rifles, pistols and other weapons through Advent of Ascensions's infusion crafting. Level-up your runation skills to craft powerful staffs which can balance any difficult fight.

![](https://www.bisecthosting.com/images/CF/Odyssey_Space/BH_OS_Action.png)

After that, the cosmos is yours to explore with over 30 bosses and countless mobs to encounter, spread out over 22 unique planets provided by AoA, Futurepack. To help you along the way there are numerous tools and utilities, like a friendly Robit from Mekanism and even some modular armor unlocked through Futurepack and Mekanism.

With turret guns and forcefields, you can make your spaceship your own portable home, complete with defense system and possible even doors that only open for you!

Begin your Odyssey across the stars!

![](https://www.bisecthosting.com/images/CF/Odyssey_Space/BH_OS_Info.png)

[Discord](https://discord.gg/eBgbVx55mM): -  Please stop in the Discord and offer any suggestions for the pack, I would love to hear from the players!

SERVER FILES ADDED to 2.2.1-R.  Let me know of any issues and I will try and fix!

WARNING: 2.2.0-R Removes Mekanism Generators and adds a LOT of quests. I recommend starting a new save or creatively completing quests you've already done using /ftbquests editing\_mode

![](https://www.bisecthosting.com/images/CF/Odyssey_Space/BH_OS_Credits.png)

Special Thanks to:

Nebula240 - providing some Tooltips

Draco4 - modpack creation advice

Raptor - for creating the amazing graphics both in-game and here!

[![](https://www.bisecthosting.com/images/CF/Odyssey_Space/BH_OS_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fdwarfhavengames)
