---
Title: 'Project Equivalence New Horizons'
Author: 'thenimbleninja'
Description: 'Project Equivalence New Horizons'
Image: 'project-equivalence-new-horizons.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/project-equivalence-new-horizons'
Categories: ['skyblock','magic','quests','tech']
---
![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Header.png)

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Overview.png)

Project Equivalence New Horizons is an expert skyblock modpack based on 1.15 in my project equivalence series.there are quite a few new mods to play with! Sadly a few mods are lacking in the pack namly topography and project ex so no final star and no custom worlds. The world gen for skyblock is done via botania garden of glass. The quest system in the pack uses ftb quests and the end game goal is creative! To start your adventure you need to create a world using the botania garden of glass world gen. Then  when you start gathering cobblestone by shift clicking the ground to get pebbles then open up your questbook in your inventory its in the upper left corner or open it via a keybind! When you open the book up  trade in the  64 cobblestone to get your starting tools. The starting tools are just like the original; they are the watch of flowing time, philosopher's stone, tome of knowledge and emc table. Just like in the first pack the tome of knowledge unlocks everything that has emc and gives you 500 emc,and just like in the first one only resources have emc. Hope you will enjoy my modpack and have fun

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Community.png)

[![](https://i.imgur.com/4k8lKdV.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.guilded.gg%252fi%252fRkrdW7lE)
[![](https://i.imgur.com/uabSTGk.png)](https://www.patreon.com/NinjaTheNimble)

 [![](https://cdn.discordapp.com/attachments/862061100933972038/865086703261974598/packring-logo.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Multiplayer.png)

**                                               Project Equivalence has partnered with BisectHosting! **

[![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https://bisecthosting.com/TheNimbleNinja)

**      BisectHosting** offers quick, simple, and high-quality server hosting with over a dozen locations available across the world, ensuring low                                             latency. After your purchase, the server is already configured and ready to use.

                       Use the code **"TheNimbleNinja"** to get **25% OFF** your first month as a new client for any of their gaming servers.
