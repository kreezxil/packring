---
Title: 'Feywilds'
Author: 'LunafeyLazuli'
Description: 'Exploring, magic, PMMO, adventure, dungeons, and creativity.'
Image: 'feywilds.jpeg'
URL: 'https://www.curseforge.com/minecraft/modpacks/feywilds'
Categories: ['exploration','magic','adventure','multiplayer']
---
![](https://cdn.discordapp.com/attachments/837099476903657482/862907917975945216/feywild_1.png)

 ***Huge thank you to Splat for the awesome artwork!!! Make sure to give her some love at [https://ko-fi.com/splattertank](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fko-fi.com%252fsplattertank) if you would like to get some commissions!***

![](https://www.bisecthosting.com/images/CF/Feywilds/BH_F_HEADER.png)

***Welcome to the wilds, Traveler. Tales have been told of these lands, of both adventure and tragedy. Something magical resides in these forests, but you seem to feel followed. Stalking you around every corner and through every trial is death.***

With MMO levels, Dungeons, Magic, Dragons, Quests, and adventure to your heart's content; there is no shortage of things to explore, pillage, collect, tame, and conquer. With great challenges comes great reward, and with patience comes success. Will you be able to tame the wilds?

***I wish you luck on your adventures Traveler and beware of the danger that surrounds you completely.***

![](https://www.bisecthosting.com/images/CF/Feywilds/BH_F_OVERVIEW.png)

Our pack mainly focuses on exploring, magic, PMMO, adventure, dungeons, and creativity. I do try to update the pack when I can, but working a full-time job usually has most of my time taken up ^-^

**Features Coming Soon:**

*   Player Shops and Economy
*   Quests
*   NPCs with Quests
*   More fun mods to play with and build

For the most part, it is still very much a work in progress, but here's a list of our main mods ^-^

(You can check out the rest [here](https://www.curseforge.com/minecraft/modpacks/feywilds/relations/dependencies))

The 1.18 Alpha version will have all of these mods in the future as well hopefully ^-^

*   [Alex's Mobs](https://www.curseforge.com/minecraft/mc-mods/alexs-mobs)
*   [Aquaculture 2](https://www.curseforge.com/minecraft/mc-mods/aquaculture)
*   [Ars Nouveau](https://www.curseforge.com/minecraft/mc-mods/ars-nouveau)
*   [Artifacts](https://www.curseforge.com/minecraft/mc-mods/artifacts)
*   [Betterend](https://www.curseforge.com/minecraft/mc-mods/betterend-forge-port)
*   [Botania](https://www.curseforge.com/minecraft/mc-mods/botania)
*   [Classes](https://www.curseforge.com/minecraft/mc-mods/classes)
*   [Cooking for Blockheads](https://www.curseforge.com/minecraft/mc-mods/cooking-for-blockheads)
*   [Corail Tombstone](https://www.curseforge.com/minecraft/mc-mods/corail-tombstone)
*   [Create](https://www.curseforge.com/minecraft/mc-mods/create)
*   [Feywild](https://www.curseforge.com/minecraft/mc-mods/feywild)
*   [FTB Chunks](https://www.curseforge.com/minecraft/mc-mods/ftb-chunks-forge)
*   [FTB Quests](https://www.curseforge.com/minecraft/mc-mods/ftb-quests-forge)
*   [Ice and Fire](https://www.curseforge.com/minecraft/mc-mods/ice-and-fire-dragons)
*   [Mana and Artifice](https://www.curseforge.com/minecraft/mc-mods/mana-and-artifice)
*   [Minecolonies](https://www.curseforge.com/minecraft/mc-mods/minecolonies)
*   [Mythic Botany](https://www.curseforge.com/minecraft/mc-mods/mythicbotany)
*   [Packed Up](https://www.curseforge.com/minecraft/mc-mods/packed-up-backpacks)
*   [Pam's HarvestCraft 2 + all add-ons](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-crops)
*   [Quark](https://www.curseforge.com/minecraft/mc-mods/quark)
*   [Quark Oddities](https://www.curseforge.com/minecraft/mc-mods/quark-oddities)
*   [Serene Seasons](https://www.curseforge.com/minecraft/mc-mods/serene-seasons)
*   [Server Utils](https://www.curseforge.com/minecraft/mc-mods/server-utilities)
*   [Supplementaries](https://www.curseforge.com/minecraft/mc-mods/supplementaries)
*   [Table Top Craft](https://www.curseforge.com/minecraft/mc-mods/table-top-craft)
*   [Waystones](https://www.curseforge.com/minecraft/mc-mods/waystones)
*   [When Dungeons Arise](https://www.curseforge.com/minecraft/mc-mods/when-dungeons-arise)
*   [Wyrmroost](https://www.curseforge.com/minecraft/mc-mods/wyrmroost)
*   [Zetter](https://www.curseforge.com/minecraft/mc-mods/zetter)

![](https://www.bisecthosting.com/images/CF/Feywilds/BH_F_COMMUNITY.png)

**Have any questions, ideas, suggestions, or did you find any bugs with our pack?**

We do all of that and more in our discord! We also host in-game server events, game nights, movie nights, double EXP events, and more!

So if you're interested in hanging out or just need some help, feel free to join: ***[Feywilds Discord](https://discord.gg/RKaRe5g)***

![](https://imgur.com/a/IMxmxTn)[![](https://i.imgur.com/EIoCbpG.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252ffeywilds)

**Enjoying the Feywilds Modpack and want to make your own server?**

We use Bisect Hosting to run ours and it has been a great joy so far, so I can happily recommend it!

If you're looking to simply play with a few friends or start a community, this is the place to be! Bisect's Hosting service is super easy and quick to set up, has amazing hosting services with a great variety of hosting locations across the globe, with low latency!

Use our code **"feywilds"** to get 25% off your first month and enjoy their wonderful services!

[**☾☆☽ https://www.bisecthosting.com/feywilds ☾☆☽**](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252ffeywilds)
